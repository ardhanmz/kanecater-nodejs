const path= require("path");
//add multer to manage multipart form
var express = require("express");
const multer= require("multer");

//storage management for the file
//that will be uploaded
var fs = require('fs');
crypto = require('crypto');

storage = multer.diskStorage({
    destination: './images/avatar',
    filename: function(req, file, cb) {
      return crypto.pseudoRandomBytes(16, function(err, raw) {
        if (err) {
          return cb(err);
        }
        return cb(null, "" + (raw.toString('hex')) + (path.extname(file.originalname)));
      });
    }
  });


 //management of the storage and the file that will be uploaded 
 //.single expects the name of the file input field
const upload= multer({storage: storage}).single("upload");

module.exports= upload;