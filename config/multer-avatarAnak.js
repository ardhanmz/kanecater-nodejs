const path= require("path");
//add multer to manage multipart form
var express = require("express");
const multer= require("multer");

//storage management for the file
//that will be uploaded
let storage = multer.diskStorage({
  destination : path.join('./public/uploads/images/avatar/data-anak'),

  filename: function(req, file, cb){

      cb(null, file.fieldname + '-' + Date.now() +

      path.extname(file.originalname));

  }
  })


 //management of the storage and the file that will be uploaded 
 //.single expects the name of the file input field
const multerAnak= multer({storage: storage}).single("anak");

module.exports= multerAnak;