var express = require("express");
var app = express();
var path  = require('path');
const mysql = require('mysql');
const jwt = require('jsonwebtoken');
var db = require('./database');
var dbfunc = require('./db-function');
var http  = require('http')
var bodyParser = require('body-parser');
var UserRoute = require('../app/routes/user.route');
var AuthenticRoute = require('../app/routes/authentic.route');
var MenuRoute = require("../app/routes/menu.route")
var UserRoute = require("../app/routes/user.route")
var OrderRoute = require("../app/routes/order.route");
var errorCode = require('../common/error-code')
var errorMessage = require('../common/error-methods')
var checkToken = require('./secureRoute');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var multer = require('multer');
var fs = require('fs');
crypto = require('crypto');
// var schedule = require('node-schedule');
 
// var j = schedule.scheduleJob('*/1 * * * *', function(){
//   console.log('The answer to life, the universe, and everything!');
// });

dbfunc.connectionCheck.then((data) =>{
    //console.log(data);
 }).catch((err) => {
     console.log(err);
 });
 
 app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("./images"));
app.use(express.static("./images/avatar"));
var router = express.Router({mergeParams: true});
app.use('/api',router);
AuthenticRoute.init(router);
MenuRoute.init(router);
UserRoute.init(router);
OrderRoute.init(router);

// var secureApi = express.Router();


//set static folder
app.use(express.static('public'));
// app.use(express.static(__dirname + '/public/images/avatar'));
// app.use('/images', express.static(__dirname + '/Images'));

//body parser middleware

// app.use('/secureApi',secureApi);
// secureApi.use(checkToken);


app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

storage = multer.diskStorage({
  destination: './uploads/avatar',
  filename: function(req, file, cb) {
    return crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) {
        return cb(err);
      }
      return cb(null, "" + (raw.toString('hex')) + (path.extname(file.originalname)));
    });
  }
});

// index route
app.get('/', (req,res) => {
    res.send('hello world');
});
var form = "<!DOCTYPE HTML><html><body>" +
"<form method='post' action='/uploadImage' enctype='multipart/form-data'>" +
"<input type='file' name='upload'/>" +
"<input type='submit' /></form>" +
"</body></html>";
app.get('/uploadImage', function (req, res){
  res.writeHead(200, {'Content-Type': 'text/html' });
  res.end(form);

});

app.post(
  "/uploadImage",
  multer({
    storage: storage
  }).single('upload'), function(req, res) {
    console.log(req.file);
    console.log(req.body);
    res.redirect("/uploads/avatar/" + req.file.filename);
    console.log(req.file.filename);
    const host = req.hostname;
    const filePath = req.protocol + "://" + host;
    console.log(filePath + "/uploads/avatar/" + req.file.filename);
    
    return res.status(200).end();
  });

var ApiConfig = {
  app: app
}
app.get('/uploads/:upload', function (req, res){
  file = req.params.upload;
  console.log(req.params.upload);
  var img = fs.readFileSync(__dirname + "/uploads/" + file);
  res.writeHead(200, {'Content-Type': 'image/png' });
  res.end(img, 'binary');

});


// Passport init
app.use(passport.initialize());
app.use(passport.session());

// UserRoute.init(secureApi);

module.exports = ApiConfig;
