

var menuModel = require("../models/menu.model");

var menuService = {
    getAllPaketLangganan: getAllPaketLangganan,
    getPaketLanggananById: getPaketLanggananById,
    getAllKategoriMasakan: getAllKategoriMasakan,
    getListMakananBypaket: getListMakananBypaket,
    getKategoriMasakanById: getKategoriMasakanById,
    getNasiByMasakan: getNasiByMasakan,
    getLaukSatuByMasakan: getLaukSatuByMasakan,
    getLaukDuaByMasakan: getLaukDuaByMasakan,
    getBuahByMasakan: getBuahByMasakan,
    getSayurByMasakan: getSayurByMasakan,
    search: search,
    getNasiAVG: getNasiAVG,
    getLaukSatuAVG: getLaukSatuAVG,
    getLaukDuaAVG: getLaukDuaAVG,
    getSayurAVG: getSayurAVG,
    getBuahAVG: getBuahAVG,
    getLanggananNasiAvg: getLanggananNasiAvg,
    getLanggananLaukSatuAVG: getLanggananLaukSatuAVG,
    getLanggananLaukDuaAvg: getLanggananLaukDuaAvg,
    getLanggananSayurAvg: getLanggananSayurAvg,
    getLanggananBuahAvg: getLanggananBuahAvg
}

function getAllPaketLangganan(){
    return new Promise((resolve, reject)=>{
        menuModel.getAllPaketLangganan().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}

function getPaketLanggananById(id){
    return new Promise((resolve, reject)=>{
        menuModel.getPaketLanggananById(id).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function getListMakananBypaket(id){
    return new Promise((resolve, reject)=>{
        menuModel.getListMakananByPaket(id).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getAllKategoriMasakan(){
    return new Promise((resolve, reject)=>{
        menuModel.getAllKategoriMasakan().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}

function getAllKategoriMasakan(){
    return new Promise((resolve, reject)=>{
        menuModel.getAllKategoriMasakan().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}

function getKategoriMasakanById(idmasakan){
    return new Promise((resolve, reject)=>{
        menuModel.getAllKategoriMasakan(idmasakan).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}

function getNasiByMasakan(idmasakan){
    return new Promise((resolve, reject)=>{
        menuModel.getNasiByMasakan(idmasakan).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLaukSatuByMasakan(idmasakan){
    return new Promise((resolve, reject)=>{
        menuModel.getLaukSatuByMasakan(idmasakan).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLaukDuaByMasakan(idmasakan){
    return new Promise((resolve, reject)=>{
        menuModel.getLaukDuaByMasakan(idmasakan).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getSayurByMasakan(idmasakan){
    return new Promise((resolve, reject)=>{
        menuModel.getSayurByMasakan(idmasakan).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getBuahByMasakan(idmasakan){
    return new Promise((resolve, reject)=>{
        menuModel.getBuahByMasakan(idmasakan).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getKomponenMenuNasi(){
    return new Promise((resolve, reject)=>{
        menuModel.getKomponenMenuNasi().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getKomponenMenuLaukSatu(){
    return new Promise((resolve, reject)=>{
        menuModel.getKomponenMenuLaukSatu().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getKomponenMenuLaukDua(){
    return new Promise((resolve, reject)=>{
        menuModel.getKomponenMenuLaukDua().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getKomponenMenuSayur(){
    return new Promise((resolve, reject)=>{
        menuModel.getKomponenMenuSayur().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getKomponenMenuBuah(){
    return new Promise((resolve, reject)=>{
        menuModel.getKomponenMenuBuah().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function search(data){
    return new Promise((resolve, reject)=>{
        menuModel.searchPaket(data).then((data)=>{
            resolve(data);
        }).catch((error)=>{
            reject(error);
        })
    })
}
function getNasiAVG(data){
    return new Promise((resolve, reject)=>{
        menuModel.getNasiAVG(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLaukSatuAVG(data){
    return new Promise((resolve, reject)=>{
        menuModel.getLaukSatuAVG(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLaukDuaAVG(data){
    return new Promise((resolve, reject)=>{
        menuModel.getLaukDuaAVG(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getSayurAVG(data){
    return new Promise((resolve, reject)=>{
        menuModel.getSayurAVG(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getBuahAVG(data){
    return new Promise((resolve, reject)=>{
        menuModel.getBuahAVG(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLanggananNasiAvg(){
    return new Promise((resolve, reject)=>{
        menuModel.getLanggananNasiAvg().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLanggananLaukSatuAVG(){
    return new Promise((resolve, reject)=>{
        menuModel.getLanggananLaukSatuAvg().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLanggananLaukDuaAvg(){
    return new Promise((resolve, reject)=>{
        menuModel.getLanggananLaukDuaAvg().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLanggananSayurAvg(){
    return new Promise((resolve, reject)=>{
        menuModel.getLanggananSayurAvg().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getLanggananBuahAvg(){
    return new Promise((resolve, reject)=>{
        menuModel.getLanggananBuahAvg().then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}

module.exports = menuService;