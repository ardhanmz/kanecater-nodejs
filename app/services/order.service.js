var orderModel = require('../models/order.model');

var orderService = {
    order: order,
    order2: order2,
    getOrder: getOrder,
    order_custom: order_custom,
    order_custom2: order_custom2,
    order_custom3: order_custom3,
    order_custom4: order_custom4,
    check_ordercustom: check_ordercustom,
    payOrder: payOrder,
    cancelOrder: cancelOrder,
    OrderByUserId: OrderByUserId,
    OrderBelumDibayar: OrderBelumDibayar,
    PengirimanOrder: PengirimanOrder,
    upDateStatusOrder: upDateStatusOrder
}

function order(data){
    return new Promise((resolve, reject)=>{
        orderModel.addorder(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function getOrder(data){
    return new Promise((resolve, reject)=>{
        orderModel.getOrder(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function order2(data){
    return new Promise((resolve, reject)=>{
        orderModel.addorder2(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}

function check_ordercustom(data){
    return new Promise((resolve, reject)=>{
        orderModel.check_ordercustom(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}

function order_custom(data){
    return new Promise((resolve, reject)=>{
        orderModel.addorder_custom(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function order_custom2(data){
    return new Promise((resolve, reject)=>{
        orderModel.addorder_custom2(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function order_custom3(data){
    return new Promise((resolve, reject)=>{
        orderModel.addorder_custom3(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function order_custom4(data){
    return new Promise((resolve, reject)=>{
        orderModel.addorder_custom4(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function payOrder(data){
    return new Promise((resolve, reject)=>{
        orderModel.payOrder(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function cancelOrder(data){
    return new Promise((resolve, reject)=>{
        orderModel.cancelOrder(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function OrderByUserId(data){
    return new Promise((resolve, reject)=>{
        orderModel.OrderByUserId(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function OrderBelumDibayar(data){
    return new Promise((resolve, reject)=>{
        orderModel.OrderBelumDibayar(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function PengirimanOrder(data){
    return new Promise((resolve, reject)=>{
        orderModel.PengirimanOrder(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function upDateStatusOrder(data){
    return new Promise((resolve, reject)=>{
        orderModel.upDateStatusOrder(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
module.exports = orderService;