var authenticModel = require("../models/authentic.model");


var authenticService = {
    authentic: authentic,
    signup:signup,
    changePassword: changePassword,
    upDateFirebaseID: upDateFirebaseID
}

function authentic(authenticData) {
    return new Promise((resolve,reject) => {
        authenticModel.authentic(authenticData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
   
}

function signup(signUpData) {
    
    return new Promise((resolve,reject) => {
        authenticModel.signup(signUpData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
   
}
function changePassword(authenticData) {
    return new Promise((resolve,reject) => {
        authenticModel.changePassword(authenticData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
}
function upDateFirebaseID(data){
    return new Promise((resolve, reject)=>{
        authenticModel.upDateFirebaseID(data).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}


module.exports = authenticService;

