var userModel = require("../models/user-model.js");


var userService = {
    getAllUser: getAllUser,
    getUserById:getUserById,
    addUser: addUser,
    updateUser:updateUser,
    updateImageProfile: updateImageProfile,
    tambah_anak: tambah_anak,
    deleteAnak: deleteAnak,
    upDateAnak: upDateAnak,
    getDetailAnakById: getDetailAnakById,
    deleteUser:deleteUser,
    getAnakByUser:getAnakByUser,
    getAnak: getAnak
}

function addUser(userData) {
    return new Promise((resolve,reject) => {
        userModel.addUser(userData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
   
}


function updateUser(id,userData,callback) {
    return new Promise((resolve,reject) => {
        userModel.updateUser(id,userData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
     
}
function upDateAnak(id,userData) {
    return new Promise((resolve,reject) => {
        userModel.upDateAnak(id,userData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
     
}

function updateImageProfile(id,avatar,callback) {
    return new Promise((resolve,reject) => {
        userModel.updateImageProfile(id,avatar).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
     
}
function tambah_anak(userData, callback){
    return new Promise((resolve, reject)=>{
        userModel.tambah_anak(userData).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function deleteAnak(id){
    return new Promise((resolve, reject)=>{
        userModel.deleteAnak(id).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function getDetailAnakById(id){
    return new Promise((resolve, reject)=>{
        userModel.getDetailAnakById(id).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    })
}
function deleteUser(id) {
    return new Promise((resolve,reject) => {
        userModel.deleteUser(id).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })
}

function getAllUser() {
    return new Promise((resolve,reject) => {
        userModel.getAllUser().then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
}

function getUserById(id) {
    return new Promise((resolve,reject) => {
        userModel.getUserById(id).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
}
function getAnakByUser(id){
    return new Promise((resolve, reject)=>{
        userModel.getAnakByUser(id).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}
function getAnak(id, paket){
    return new Promise((resolve, reject)=>{
        userModel.getAnak(id,paket).then((data)=>{
            resolve(data);
        }).catch((err)=>{
            reject(err);
        })
    });
}


module.exports = userService;

