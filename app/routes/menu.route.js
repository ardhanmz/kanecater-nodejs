const menuService = require("../services/menu.service");

function init (router){
    router.route('/menu/paket/search').get(search);
    router.route('/menu/paket/langganan/all').get(getAllPaket);
    router.route('/menu/paket/langganan/:id').get(getPaketById);
    router.route('/menu/paket/custom/all').get(getAllMasakan);
    router.route('/menu/paket/custom/:id').get(getKategoriMasakanById);
    router.route('/menu/paket/custom/komponen/nasi/:idmasakan').get(getNasiByMasakan);
    router.route('/menu/paket/custom/komponen/lauksatu/:idmasakan').get(getLaukSatuByMasakan);
    router.route('/menu/paket/custom/komponen/laukdua/:idmasakan').get(getLaukDuaByMasakan);
    router.route('/menu/paket/custom/komponen/sayur/:idmasakan').get(getSayurByMasakan);
    router.route('/menu/paket/custom/komponen/dessert/:idmasakan').get(getBuahByMasakan);
    router.route('/menu/paket/custom/komponen/nasi/average/:idmasakan').get(getAvgNasi);
    router.route('/menu/paket/custom/komponen/lauksatu/average/:idmasakan').get(getLaukSatuAVG);
    router.route('/menu/paket/custom/komponen/laukdua/average/:idmasakan').get(getLaukDuaAvg);
    router.route('/menu/paket/custom/komponen/sayur/average/:idmasakan').get(getSayurAvg);
    router.route('/menu/paket/custom/komponen/buah/average/:idmasakan').get(getBuahAvg);
    router.route('/menu/paket/langganan/nasi/average').get(getLanggananNasiAvg);
    router.route('/menu/paket/langganan/lauksatu/average').get(getLanggananLaukSatuAvg);
    router.route('/menu/paket/langganan/laukdua/average').get(getLanggananLaukDuaAvg);
    router.route('/menu/paket/langganan/sayur/average').get(getLanggananSayurAvg);
    router.route('/menu/paket/langganan/buah/average').get(getLanggananBuahAvg);
    router.route('/search').get(search);
}

function getAllPaket(req,res) {
    menuService.getAllPaketLangganan().then((data) => {
        res.send(data);
      }).catch((err) => {
        mail.mail(err);
        res.send(err);
      });
  }
function getPaketById(req, res){
    let idpaket = req.params.id;
    menuService.getPaketLanggananById(idpaket).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getListMakananByPaket(req, res){
    var idpaket = req.params;
    menuService.getListMakananBypaket(idpaket).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getAllMasakan(req, res){
    menuService.getAllKategoriMasakan().then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getKategoriMasakanById(req, res){
    let idmasakan = req.params;
    menuService.getKategoriMasakanById(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getNasiByMasakan(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getNasiByMasakan(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getLaukSatuByMasakan(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getLaukSatuByMasakan(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getLaukDuaByMasakan(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getLaukDuaByMasakan(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getSayurByMasakan(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getSayurByMasakan(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getBuahByMasakan(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getBuahByMasakan(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function search(req, res){
    let data= req.query.data;
    menuService.search(data).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getAvgNasi(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getNasiAVG(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getLaukSatuAVG(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getLaukSatuAVG(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getLaukDuaAvg(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getLaukDuaAVG(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getSayurAvg(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getSayurAVG(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getBuahAvg(req, res){
    let idmasakan= req.params.idmasakan;
    menuService.getBuahAVG(idmasakan).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
        mail.mail(err);
    })
}
function getLanggananNasiAvg(req, res){
    let idpaket = req.params.id;
    menuService.getLanggananNasiAvg().then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getLanggananLaukSatuAvg(req, res){
    let idpaket = req.params.id;
    menuService.getLanggananLaukSatuAVG().then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getLanggananLaukDuaAvg(req, res){
    let idpaket = req.params.id;
    menuService.getLanggananLaukDuaAvg().then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getLanggananSayurAvg(req, res){
    let idpaket = req.params.id;
    menuService.getLanggananSayurAvg().then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}
function getLanggananBuahAvg(req, res){
    let idpaket = req.params.id;
    menuService.getLanggananBuahAvg().then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
}

module.exports.init = init;