const orderService = require('../services/order.service');
var mail = require('./../../common/mailer.js');
function init (router){
  router.route('/order/custom/add').post(addOrder);
  router.route('/order/langganan/add').post(addOrder2);
  router.route('/order/check/custom/:id').get(getOrder);
  router.route('/order/custom/paket/full').post(addOrderCustom);
  router.route('/order/custom/paket/empat').post(addOrderCustom4);
  router.route('/order/custom/paket/tiga').post(addOrderCustom3);
  router.route('/order/custom/paket/dua').post(addOrderCustom2);
  router.route('/order/custom/check').post(checkOrderCustom);
  router.route('/order/cancel').post(cancelOrder);
  router.route('/order/payment/add').post(paymentOrder);
  router.route('/order/all/user/:id').get(OrderByUserId);
  router.route('/order/all/delivery/progress/:id').get(PengirimanOrder);
  router.route('/order/all/payment/notpaid/user/:id').get(orderBelumDibayar);
  router.route('/order/payment/update').post(upDateStatusOrder);
    router.route('/addOrder').post(addOrder);
    router.route('/addPaketOrder').post(addOrder2);
    router.route('/addCustomOrder').post(addOrderCustom);
    router.route('/addCustomOrder2').post(addOrderCustom2);
    router.route('/addCustomOrder3').post(addOrderCustom3);
    router.route('/addCustomOrder4').post(addOrderCustom4);
    router.route('/checkOrderCustom').post(checkOrderCustom);
    router.route('/paymentOrder').post(paymentOrder);
    router.route('/cancelOrder').post(cancelOrder)
    router.route('/orderByUser/:id').get(OrderByUserId);
    router.route('/orderBelumDibayar/:id').get(orderBelumDibayar).post(upDateStatusOrder);
    router.route('/pengirimanOrder/:id').get(PengirimanOrder);
}
function addOrder(req,res) {
    var orderData=req.body;  
     orderService.order(orderData).then((data) => {
      if(data) {
         res.json({
           "success":true,
           "data":data,
           "message": "Pemesanan Telah Berhasil"
         });
       }
     }).catch((err) => {
       res.json(err);
     });
  }
  function getOrder(req,res) {
    var id = req.params.id;
     orderService.getOrder(id).then((data) => {
      if(data) {
         res.send(data);
       }
     }).catch((err) => {
       mail.mail(err);
       res.json(err);
     });
  }
  function addOrder2(req,res) {
      var orderData=req.body;  
       orderService.order2(orderData).then((data) => {
        if(data) {
           res.json({
             "success":true,
             "data":data,
             "message": "Pemesanan Telah Berhasil"
           });
         }
       }).catch((err) => {
         res.json(err);
       });
    
    }
  function addOrderCustom(req,res) {
    var orderData=req.body;
    
    //Validating the input entity
    //  var json_format = iValidator.json_schema(schema.postSchema, signUpData, "signUpData");
    //  if (json_format.valid == false) {
    //    return res.status(422).send(json_format.errorMessage);
    //  }
  
     orderService.order_custom(orderData).then((data) => {
      if(data) {
        res.json({
          "success":false,
          "data":data,
          "message": "Berhasil Menambahkan Menu"
        });
       }
     }).catch((err) => {
       res.json(err);
     });
  
  }
  function addOrderCustom2(req,res) {
    var orderData=req.body;
     orderService.order_custom2(orderData).then((data) => {
      if(data) {
        res.json({
          "success":false,
          "data":data,
          "message": "Berhasil Menambahkan Menu"
        });
       }
     }).catch((err) => {
       res.json(err);
     });
  }
  function addOrderCustom3(req,res) {
    var orderData=req.body;
    orderService.order_custom3(orderData).then((data) => {
      if(data) {
        res.json({
          "success":true,
          "data":data,
          "message": "Berhasil Menambahkan Menu"
        });
       }
     }).catch((err) => {
       res.json(err);
     });
  }
  function addOrderCustom4(req,res) {
    var orderData=req.body;
     orderService.order_custom4(orderData).then((data) => {
      if(data) {
         res.json({
           "success":true,
           "data":data,
           "message": "Berhasil Menambahkan Menu"
         });
       }
     }).catch((err) => {
       res.json(err);
     });
  }
  function checkOrderCustom(req,res) {
    var orderData=req.body;
     orderService.check_ordercustom(orderData).then((data) => {
      if(data) {
         res.send(data);
       }
     }).catch((err) => {
       mail.mail(err);
       res.json(err);
     });
  }
  function paymentOrder(req,res) {
    var orderData=req.body;
     orderService.payOrder(orderData).then((data) => {
      if(data) {
         res.send(data);
       }
     }).catch((err) => {
       res.json(err);
     });
  }
  function cancelOrder(req,res) {
    var orderData=req.body;
     orderService.cancelOrder(orderData).then((data) => {
      if(data) {
        res.json({
          "success":true,
          "data":data,
          "message": "Order Telah Dibatalkan"
        });
       }
     }).catch((err) => {
       res.json(err);
     });
  }
  function OrderByUserId(req,res) {
    var id = req.params;
     orderService.OrderByUserId(id).then((data) => {
      if(data) {
         res.send(data);
       }
     }).catch((err) => {
       mail.mail(err);
       res.json(err);
     });
  }
  function orderBelumDibayar(req,res) {
    var orderData=req.body;
    var id = req.params;
     orderService.OrderBelumDibayar(id).then((data) => {
      if(data) {
         res.send(data);
       }
     }).catch((err) => {
       mail.mail(err);
       res.json(err);
     });
  }
  function PengirimanOrder(req,res) {
    var orderData=req.body;
    var id = req.params;
     orderService.PengirimanOrder(id).then((data) => {
      if(data) {
         res.send(data);
       }
     }).catch((err) => {
       mail.mail(err);
       res.json(err);
     });
  }
  function upDateStatusOrder(req, res){
    var orderData = req.body;
    var idorder = req.params;
    orderService.upDateStatusOrder(orderData).then((data)=>{
      res.send(data);
    }).catch((err)=>{
      mail.mail(err);
      res.json(err);
    })
  }
  module.exports.init = init;