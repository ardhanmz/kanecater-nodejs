const userService = require('../services/user.service');
var schema = require('../schema/userValidationSchema.json')
var iValidator = require('../../common/iValidator');
var errorCode = require('../../common/error-code');
var errorMessage = require('../../common/error-methods');
var mail = require('./../../common/mailer.js');
const upload= require("../../config/multer");
const multerAnak = require("../../config/multer-avatarAnak");

var multer = require('multer');
var fs = require('fs');



function init(router) {
    router.route('/user/uploadimage/:id').get(blabla).post(updateImageProfile);
    router.route('/user/anak/:id').get(getAnakByUser);
    router.route('/user/anak/detail/:id').post(getAnak);
    router.route('/user/anak/add').post(tambah_anak);
    router.route('/user/anak/delete/:id').delete(deleteAnak);
    router.route('/user/anak/edit/:id').put(updateAnak);  
}
function getAnak(req,res){
  let idanak = req.params.id;
  var body = req.body;
  userService.getAnak(idanak,body).then((data) => {
      res.json(data);
    }).catch((err) => {
      mail.mail(err);
      res.send(err);
    });
}
function getAnakByUser(req,res){
  let userId = req.params;

  // var json_format = iValidator.json_schema(schema.getSchema,userId,"user");
  // if (json_format.valid == false) {
  //   return res.status(422).send(json_format.errorMessage);
  // }

  userService.getAnakByUser(userId).then((data) => {
      res.send(data);
    }).catch((err) => {
      mail.mail(err);
      res.send(err);
    });
}
function getAllUsers(req,res) {
  userService.getAllUser().then((data) => {
      res.send(data);
    }).catch((err) => {
      mail.mail(err);
      res.send(err);
    });
}

function getUserById(req,res) {

  let userId = req.params;

  var json_format = iValidator.json_schema(schema.getSchema,userId,"user");
  if (json_format.valid == false) {
    return res.status(422).send(json_format.errorMessage);
  }

  userService.getUserById(userId).then((data) => {
      res.send(data);
    }).catch((err) => {
      mail.mail(err);
      res.send(err);
    });
}

function addUser(req,res) {
  var userData=req.body;
  
  //Validating the input entity
   var json_format = iValidator.json_schema(schema.postSchema, userData, "user");
   if (json_format.valid == false) {
     return res.status(422).send(json_format.errorMessage);
   }

  userService.addUser(userData).then((data) => {
    res.json(data);
  }).catch((err) => {
    mail.mail(err);
    res.json(err);
  });

}


function updateUser(req,res) {
   var userData=req.body;
   var id = req.params.id;
   userService.updateUser(id,userData).then((data)=>{
      res.json(data);
  }).catch((err)=>{
      mail.mail(err);
      res.json(err);
   });
}
function updateAnak(req,res) {
  var userData=req.body;
  var id = req.params.id;
  userService.upDateAnak(id,userData).then((data)=>{
    res.send(data)
 }).catch((err)=>{
     mail.mail(err);
     res.json(err);
  });
}

function updateImageProfile(req,res) {
  var avatar = req.file; 
 
  var userData=req.body;
  var id = req.params;
  upload(req, res, err =>{
    console.log(req.file);
    console.log(req.body);
    console.log(req.file.filename);
    console.log();
    
    const host = req.hostname;
    var filePath = req.protocol + "://" + host +"/"+ req.file.filename;
    console.log(filePath);
    // res.redirect(filePath);
    userService.updateImageProfile(id,filePath).then((data)=>{
      res.json(data);
  }).catch((err)=>{
      mail.mail(err);
      res.json(err);
   });
  })
}

function tambah_anak(req, res){
  var dataAnak = req.body;
  var avatar = req.file; 
  // var idpengguna = req.params;
  upload(req,res,err => {
    userService.tambah_anak(dataAnak).then((data)=>{
      res.json(data);
    }).catch((err)=>{
      mail.mail(err);
      res.json(err);
    })
  })
}

function deleteUser(req,res) {
  var delId = req.params.id;
  userService.deleteUser(delId).then((data)=>{
    res.json(data);
  }).catch((err)=>{
     mail.mail(err);
      res.json(err);
  });
}
function deleteAnak(req,res) {
  var delId = req.params.id;
  userService.deleteAnak(delId).then((data)=>{
    res.send(data)
  }).catch((err)=>{
      res.json(err);
  });
}
var form = "<!DOCTYPE HTML><html><body>" +
"<form method='post' action='/api/user/uploadimage/1' enctype='multipart/form-data'>" +
"<input type='file' name='upload'/>" +
"<input type='submit' /></form>" +
"</body></html>";

function blabla(req, res){
  res.writeHead(200, {'Content-Type': 'text/html' });
  res.end(form);
}



module.exports.init = init;



