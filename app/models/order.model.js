var db = require('../../config/database');
var dbFunc = require('../../config/db-function');
var admin = require("firebase-admin");

var serviceAccount = require("../../kanecater-android-firebase-adminsdk-so57z-6ad73ebd07.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://kanecater-android.firebaseio.com"
});

var orderModel = {
    addorder: addorder,
    addorder2: addorder2,
    getOrder: getOrder,
    addorder_custom: addorder_custom,
    addorder_custom2: addorder_custom2,
    addorder_custom3: addorder_custom3,
    addorder_custom4: addorder_custom4,
    check_ordercustom: check_ordercustom,
    payOrder: payOrder,
    cancelOrder: cancelOrder,
    OrderByUserId:OrderByUserId,
    OrderBelumDibayar: OrderBelumDibayar,
    PengirimanOrder: PengirimanOrder,
    upDateStatusOrder: upDateStatusOrder
}
function addorder(userData){
    return new Promise((resolve, reject)=>{
        db.query("INSERT INTO `data_order`(`fk_idanak`,`fk_idpengguna`,`langganan_awal`,`langganan_akhir`,`alamat_order`,`jam_order`,`fk_idpaket`,`fk_idpembayaran`,`status_pembayaran`,`status_order`)VALUES('"+ userData.fk_idanak +"','"+ userData.fk_idpengguna +"','"+userData.langganan_awal+"','"+userData.langganan_akhir+"','"+userData.alamat_order+"','"+userData.jam_order+"','"+userData.fk_idpaket+"','1','Belum Dibayar','Tidak Aktif')", (err, rows, fields)=>{
            if(err){
                dbFunc.connectionRelease;
                console.log(err);
                reject(err);
            }else{
                dbFunc.connectionRelease;
                resolve(rows.insertId);
                console.log(rows.insertId);
            }
        })
    })
}
function getOrder(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_order WHERE iddata_order="+data, (err, rows, fields)=>{
            if(err){
                dbFunc.connectionRelease;
                console.log(err);
                reject(err);
            }else{
                dbFunc.connectionRelease;
                resolve(rows[0]);
            }
        })
    })
}
function addorder2(userData){
    return new Promise((resolve, reject)=>{
        db.query("INSERT INTO `data_order`(`fk_idanak`,`fk_idpengguna`,`langganan_awal`,`langganan_akhir`,`nama_paket`,`fk_idpaket`)VALUES('"+ userData.fk_idanak +"','"+ userData.fk_idpengguna +"','"+userData.langganan_awal+"','"+userData.langganan_akhir+"','"+userData.nama_paket+"','"+userData.fk_idpaket+"')", (err, rows, fields)=>{
            if(err){
                dbFunc.connectionRelease;
                console.log(err);
                reject(err);
            }else{
                dbFunc.connectionRelease;
                resolve(rows.insertId);
                console.log("HSsad"+rows.insertId);
            }
        })
    })
}
function check_ordercustom(data){
    return new Promise((reject, resolve)=>{
        db.query(`SELECT paket_custom.kalori AS kalori,menu_nasi.nama_menu AS NASI, menu_lauksatu.nama_menu AS LAUK_SATU, menu_laukdua.nama_menu AS LAUK_DUA, 
        menu_sayur.nama_menu AS SAYUR, menu_buah.nama_menu AS BUAH FROM paket_custom
        JOIN menu_nasi ON paket_custom.fk_idmenu_nasi = menu_nasi.idmenu_nasi
        JOIN menu_lauksatu ON paket_custom.fk_idmenu_lauksatu = menu_lauksatu.idmenu_lauksatu
        JOIN menu_laukdua ON paket_custom.fk_idmenu_laukdua = menu_laukdua.idmenu_laukdua
        JOIN menu_sayur ON paket_custom.fk_idmenu_sayur = menu_sayur.idmenu_sayur
        JOIN menu_buah ON paket_custom.fk_idmenu_buah = menu_buah.idmenu_buah where fk_iddata_order = '${data.iddata_order}' AND tanggal = '${data.tanggal}'` , (error, rows, fields) => {
            if (error) {
                dbFunc.connectionRelease;
                            console.log(error);
                            reject(error);
            }else{
                dbFunc.connectionRelease;
                if(rows.length<1){
                    db.query(`SELECT paket_custom.kalori AS kalori,menu_nasi.nama_menu AS NASI, menu_lauksatu.nama_menu AS LAUK_SATU, menu_laukdua.nama_menu AS LAUK_DUA, 
                    menu_sayur.nama_menu AS SAYUR FROM paket_custom
                    JOIN menu_nasi ON paket_custom.fk_idmenu_nasi = menu_nasi.idmenu_nasi
                    JOIN menu_lauksatu ON paket_custom.fk_idmenu_lauksatu = menu_lauksatu.idmenu_lauksatu
                    JOIN menu_laukdua ON paket_custom.fk_idmenu_laukdua = menu_laukdua.idmenu_laukdua
                    JOIN menu_sayur ON paket_custom.fk_idmenu_sayur = menu_sayur.idmenu_sayur where fk_iddata_order = '${data.iddata_order}' AND tanggal = '${data.tanggal}'` , (error, row1, fields) =>{
                        if(error){
                            dbFunc.connectionRelease;
                            console.log(error);
                            reject(error);
                        }else{
                            dbFunc.connectionRelease
                            if(row1.length<1){
                                db.query(`SELECT paket_custom.kalori AS kalori,menu_nasi.nama_menu AS NASI, menu_lauksatu.nama_menu AS LAUK_SATU, menu_laukdua.nama_menu AS LAUK_DUA FROM paket_custom
                                JOIN menu_nasi ON paket_custom.fk_idmenu_nasi = menu_nasi.idmenu_nasi
                                JOIN menu_lauksatu ON paket_custom.fk_idmenu_lauksatu = menu_lauksatu.idmenu_lauksatu
                                JOIN menu_laukdua ON paket_custom.fk_idmenu_laukdua = menu_laukdua.idmenu_laukdua where fk_iddata_order = '${data.iddata_order}' AND tanggal = '${data.tanggal}'` , (error, row2, fields) =>{
                                    if(error){
                                        dbFunc.connectionRelease;
                                        console.log(error);
                                        reject(error);
                                    }
                                    else {
                                        dbFunc.connectionRelease;
                                        if(row2[0]<0){
                                            reject({"status":false,"jahah": "jahaha"})
                                        }else{
                                            console.log({"row2": row2[0]});
                                            resolve(row2[0]);
                                        }
                                        
                                    }
                                })
                            }else {
                                console.log({"row1":"row1"});
                                resolve(row1[0]);
                            }
                        }
                    })
                }else {
                    console.log({"rows": "rows"});
                    resolve(rows[0])
                }
            }
        })
    });
    
}
function addorder_custom(data){
    return new Promise((reject, resolve)=>{
        db.query("INSERT INTO paket_custom(fk_idpaket,Tanggal,fk_idmenu_nasi,fk_idmenu_lauksatu,fk_idmenu_laukdua,fk_idmenu_sayur,fk_idmenu_buah,fk_iddata_order,fk_idpengguna,kalori)VALUES('" + data.fk_idpaket+ "','" + data.tanggal + "', '" + data.fk_idmenu_nasi + "', '" + data.fk_idmenu_lauksatu + "', '" + data.fk_idlaukdua + "', '" + data.fk_idmenu_sayur + "', '" + data.fk_idmenu_buah + "', '" + data.fk_iddata_order + "','" + data.kalori + "')", (error, rows, fields) => {
            if (error) {
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
            }
        });
    })
}
function  addorder_custom2(data){
    return new Promise((reject, resolve)=>{
        db.query("INSERT INTO paket_custom(fk_idpaket,Tanggal,fk_idmenu_nasi,fk_idmenu_lauksatu,fk_iddata_order, kalori)VALUES('" + data.fk_idpaket+ "','" + data.tanggal + "', '" + data.fk_idmenu_nasi + "', '" + data.fk_idmenu_lauksatu + "', '"  + data.fk_iddata_order + "','" + data.kalori + "')", (err, rows, fields) => {
            if (err) {
                dbFunc.connectionRelease;
                reject(err);
                console.log(err);
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Menu Telah Berhasil Ditambahkan"});
            }
        });
    })
}
function addorder_custom3(data){
    return new Promise((reject, resolve)=>{
        db.query("INSERT INTO paket_custom(`fk_idpaket`,`Tanggal`,`fk_idmenu_nasi`,`fk_idmenu_lauksatu`,`fk_idmenu_laukdua`,`fk_iddata_order`, kalori)VALUES('" + data.fk_idpaket+ "','" + data.tanggal + "', '" + data.fk_idmenu_nasi + "', '" + data.fk_idmenu_lauksatu + "', '" + data.fk_idmenu_laukdua + "', '" + data.fk_iddata_order + "','" + data.kalori + "')", (error, rows, fields) => {
            if (error) {
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Menu Telah Berhasil Ditambahkan"});
            }
        });
    })
}
function addorder_custom4(data){
    return new Promise((reject, resolve)=>{
        db.query("INSERT INTO paket_custom(fk_idpaket,Tanggal,fk_idmenu_nasi,fk_idmenu_lauksatu,fk_idmenu_laukdua,fk_idmenu_sayur,fk_iddata_order, kalori)VALUES('" + data.fk_idpaket+ "','" + data.tanggal + "', '" + data.fk_idmenu_nasi + "', '" + data.fk_idmenu_lauksatu + "', '" + data.fk_idmenu_laukdua + "', '" + data.fk_idmenu_sayur + "', '" + data.fk_iddata_order + "','" + data.kalori + "')", (error, rows, fields) => {
            if (error) {
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
                
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Menu Telah Berhasil Ditambahkan"});
            }
        });
    })
}
function payOrder(data){
    return new Promise((reject, resolve)=>{
        db.query("UPDATE data_order set fk_idpembayaran='"+data.fk_idpembayaran+"',status_order='Aktif' WHERE iddata_order='"+data.iddata_order+"'", (error, rows, fields) => {
            if (error) {
                dbFunc.connectionRelease;
                console.log(error);
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Order Telah Berhasil Dilakukan"});
                console.log("hehe");
            }
        });
    })    
}
function cancelOrder(data){
    return new Promise((reject, resolve)=>{
        db.query("UPDATE data_order set status_order='Tidak Aktif',status_pembayaran='"+data.status_pembayaran+"' WHERE iddata_order='"+data.iddata_order+"'", (error, rows, fields) => {
            if (error) {
                dbFunc.connectionRelease;
                console.log(error);
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Order Telah Dibatalkan"});
            }
        });
    }) 
}
function OrderByUserId(data){
    return new Promise((resolve, reject)=>{
        db.query(`SELECT da.iddata_order, da.langganan_awal, da.langganan_akhir, 
        da.status_pembayaran, da.status_order,dan.nama_anak, dpe.username, mp.metode_pembayaran, 
        dp.nama_paket, dp.tipe_paket, dp.image_paket, dp.harga_paket
        FROM data_order da JOIN data_paket dp ON da.fk_idpaket = dp.id_paket
        JOIN data_anak dan ON da.fk_idanak = dan.id_anak
        JOIN data_pengguna dpe ON da.fk_idpengguna = dpe.id_pengguna
        JOIN metode_pembayaran mp ON da.fk_idpembayaran = mp.idmetode_pembayaran WHERE dpe.id_pengguna='${data.id}' ORDER BY iddata_order DESC`  ,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function OrderBelumDibayar(data){
    return new Promise((resolve, reject)=>{
        db.query(`SELECT da.iddata_order, da.langganan_awal, da.langganan_akhir, 
        da.status_pembayaran, dan.nama_anak, dpe.username, mp.metode_pembayaran, 
        dp.nama_paket, dp.tipe_paket, dp.image_paket, dp.harga_paket
        FROM data_order da INNER JOIN data_paket dp ON da.fk_idpaket = dp.id_paket
        INNER JOIN data_anak dan ON da.fk_idanak = dan.id_anak
        INNER JOIN data_pengguna dpe ON da.fk_idpengguna = dpe.id_pengguna
        INNER JOIN metode_pembayaran mp ON da.fk_idpembayaran = mp.idmetode_pembayaran WHERE da.status_pembayaran = 'BELUM DIBAYAR' AND da.status_order = 'AKTIF' AND dpe.id_pengguna='${data.id}'` ,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
                
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
                console.log();
                
            }
        })
    })
}
function PengirimanOrder(data){
    return new Promise((resolve, reject)=>{
        db.query(`SELECT da.iddata_order, da.langganan_awal, da.langganan_akhir, 
        da.status_pembayaran, da.status_order,da.progres_order, dan.nama_anak, dpe.username, mp.metode_pembayaran, 
        dp.nama_paket, dp.tipe_paket, dp.image_paket, dp.harga_paket
        FROM data_order da INNER JOIN data_paket dp ON da.fk_idpaket = dp.id_paket
        INNER JOIN data_anak dan ON da.fk_idanak = dan.id_anak
        INNER JOIN data_pengguna dpe ON da.fk_idpengguna = dpe.id_pengguna
        INNER JOIN metode_pembayaran mp ON da.fk_idpembayaran = mp.idmetode_pembayaran WHERE da.status_pembayaran = 'Dibayar' AND da.status_order = 'AKTIF' AND dpe.id_pengguna='${data.id}'` ,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
                
            }
        })
    })
}
function upDateStatusOrder(data){
    return new Promise((resolve, reject)=>{
        db.query(`SELECT data_order.iddata_order, data_pengguna.id_pengguna, data_pengguna.username, data_pengguna.device_id FROM data_order JOIN data_pengguna ON data_order.fk_idpengguna = data_pengguna.id_pengguna WHERE data_pengguna.id_pengguna='${data.idpengguna}' AND data_order.iddata_order= '${data.iddata_order}' `,(err,rows)=>{
            if(err){
                dbFunc.connectionRelease;
                reject(err);
                console.log(err);
            } else {
                dbFunc.connectionRelease;
                var devid = rows[0].device_id;
                db.query("UPDATE data_order set status_pembayaran='Dibayar',progres_order='"+data.progres+"' WHERE iddata_order='"+data.iddata_order+"'",(err,rows)=>{
                    if(err){
                        dbFunc.connectionRelease;
                        reject(err);
                        console.log(err);
                    }else{
                        var payload = {
                            notification: {
                              title: "Data Order Telah Berhasil Di Perbarui",
                              body: "Order '"+data.progres+"'"
                            }
                          };
                          
                           var options = {
                            priority: "high",
                            timeToLive: 60 * 60 *24
                          };
                          admin.messaging().sendToDevice(devid, payload, options)
                          .then(function(response) {
                            console.log("Successfully sent message:", response);
                          })
                          .catch(function(error) {
                            console.log("Error sending message:", error);
                          });
                        resolve(rows)
                    }
                })
            }
        })
    })
}

module.exports = orderModel;