/*jshint esversion: 6 */
var db = require('../../config/database');
var dbFunc = require('../../config/db-function');

var userModel = {
   getAllUser:getAllUser,
   addUser:addUser,
   updateUser:updateUser,
   updateImageProfile: updateImageProfile,
   tambah_anak: tambah_anak,
   deleteAnak, deleteAnak,
   deleteUser:deleteUser,
   getDetailAnakById: getDetailAnakById,
   upDateAnak: upDateAnak,
   getUserById:getUserById,
   getAnakByUser:getAnakByUser,
   getAnak: getAnak,
   pushDeviceId, pushDeviceId
}

function getAllUser() {
    return new Promise((resolve,reject) => {
        db.query(`SELECT * FROM data_pengguna`,(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });
    });
}
function pushDeviceId(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_pengguna WHERE id_pengguna ="+id.id,(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                db.query("UPDATE data_pengguna SET device_id="+data, (err, rows)=>{
                    if(err){
                        dbFunc.connectionRelease;
                        reject(err);
                    } else{
                        resolve(rows);
                        console.log(rows);
                    }
                })
            }
       });
    })
}

function getUserById(id) {
    return new Promise((resolve,reject) => {
        db.query("SELECT * FROM data_pengguna WHERE id_pengguna ="+id.id,(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });
    });  
}

function addUser(user) {
     return new Promise((resolve,reject) => {
         db.query("INSERT INTO test(name,age,state,country)VALUES('"+user.name+"','"+user.age+"','"+user.state+"','"+user.country+"')",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
          });
        });
}


function updateUser(id,user) {
    return new Promise((resolve,reject) => {
        db.query("UPDATE test set name='"+user.name+"',age='"+user.age+"',state='"+user.state+"',country='"+user.country+"' WHERE id='"+id+"'",(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       });    
    })
}

function updateImageProfile(id,user) {
    return new Promise((resolve,reject) => {
        db.query("UPDATE pengguna set image_profile='"+user+"' WHERE idpengguna='"+id.id+"'",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
                
            } else{
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
                
            }
       });    
    })
}

function tambah_anak(userData){
    return new Promise((resolve, reject)=>{
        db.query("INSERT INTO data_anak(nama_anak,tanggal_lahir,jenis_kelamin,berat_badan,tinggi_badan,asal_sekolah,alamat_sekolah,kelas,fk_idpengguna)VALUES('"+ userData.nama_anak +"','"+userData.tanggal_lahir+"','"+userData.jenis_kelamin+"','"+userData.berat_badan+"','"+userData.tinggi_badan+"','"+userData.asal_sekolah+"','"+userData.alamat_sekolah+"','"+userData.kelas+"','"+userData.fk_idpengguna+"')", (err, rows, fields)=>{
            if(err){
                dbFunc.connectionRelease;
                console.log(err);
                reject(err);
            }else{
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Data Anak Telah Ditambahkan"});
            }
        })
    })
}

function getAnak(id, paket){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_anak WHERE id_anak="+id,(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
            } else {
                dbFunc.connectionRelease;
                if(rows[0]!=null){
                    if(paket.idpaket!=0){
                        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_nasi WHERE fk_id_paket="+paket.idpaket,(error, nasi, fields)=>{
                            if(error){
                                dbFunc.connectionRelease;
                                reject(error);
                            }else{
                                dbFunc.connectionRelease;
                                db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_lauksatu WHERE fk_id_paket="+paket.idpaket,(error, lauksatu, fields)=>{
                                    if(error){
                                        dbFunc.connectionRelease;
                                        reject(error);
                                    }else{
                                        dbFunc.connectionRelease;
                                        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_laukdua WHERE fk_id_paket="+paket.idpaket,(error, laukdua, fields)=>{
                                            if(error){
                                                dbFunc.connectionRelease;
                                                reject(error);
                                            }else{
                                                dbFunc.connectionRelease;
                                                db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_sayur WHERE fk_id_paket="+paket.idpaket,(error, sayur, fields)=>{
                                                    if(error){
                                                        dbFunc.connectionRelease;
                                                        reject(error);
                                                    }else{
                                                        dbFunc.connectionRelease;
                                                        resolve({"anakresponse":rows[0], "nasi":nasi[0], "lauksatu":lauksatu[0], "laukdua":laukdua[0], "sayur":sayur[0]});
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }else{
                        resolve({"anakresponse":rows[0]});
                        console.log(rows[0]);
                    }
                    
                }
                else{
                    reject("Data Tidak Ditemukan");
                }
            }
       });   
    })
}



function deleteUser(id) {
   return new Promise((resolve,reject) => {
        db.query("DELETE FROM test WHERE id='"+id+"'",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Data Berhasil Dihapus"});
            }
       });    
    });
}
function getAnakByUser(id){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_pengguna JOIN data_anak ON data_pengguna.id_pengguna=data_anak.fk_idpengguna WHERE data_pengguna.id_pengguna="+id.id,(error,rows,fields)=>{
            if(error){
                dbFunc.connectionRelease;
                console.log(error);
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        });
    });
}

function upDateAnak(id,dataAnak){
    return new Promise((resolve, reject) =>{
        db.query("UPDATE data_anak set nama_anak='"+dataAnak.nama_anak+"',tanggal_lahir='"+dataAnak.tanggal_lahir+"', tinggi_badan='"+dataAnak.tinggi_badan+"',berat_badan='"
        +dataAnak.berat_badan+"',asal_sekolah='"+dataAnak.asal_sekolah+"',alamat_sekolah='"+dataAnak.alamat_sekolah+"',kelas='"+dataAnak.kelas+"'WHERE id_anak='"+id+"'",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject({"success":false, "message":"Gagal Menghapus, Mungkin Data Anak Terkait Dengan Data Pemesanan"});
                console.log(error)
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Data Berhasil Diperbarui"});
                console.log(rows);
            }
       });   
    })
}

function deleteAnak(id){
    return new Promise((resolve, reject)=>{
        db.query("DELETE FROM data_anak WHERE id_anak='"+id+"'",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject({"success":false, "message":"Gagal Menghapus, Mungkin Data Anak Terkait Dengan Data Pemesanan"});
                console.log(error);
                
            } else {
                dbFunc.connectionRelease;
                resolve({"success":true,"message":"Data Berhasil Dihapus"});
                console.log("Wlwlwlwl");
                
            }
       }); 
    });
}
function getDetailAnakById(id){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_anak WHERE id_anak='"+id+"'",(error,rows,fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
            }
       }); 
    })
}
module.exports = userModel;

