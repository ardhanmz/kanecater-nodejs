/*jshint esversion: 6 */
var db = require('../../config/database');
var dbFunc = require('../../config/db-function');
const bcrypt = require('bcrypt');

var authenticModel = {
    authentic: authentic,
    signup: signup,
    changePassword: changePassword,
    upDateFirebaseID: upDateFirebaseID
};

function getAllPaket() {
    return new Promise((resolve,reject) => {
        db.query(`SELECT * FROM table_paket`,(error,rows,fields)=>{
            if(!!error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows[0]);
            }
       });
    });
}
function upDateFirebaseID(data){
    return new Promise((resolve, reject)=>{
        db.query(`SELECT * FROM data_pengguna WHERE username ='${data.username}'`, (error, rows, fields) => {
            if(error){
                console.log(error);
                reject(error);
            }else {
                db.query("UPDATE data_pengguna set device_id='"+data.device_id+"' WHERE username='"+data.username+"'",(err, rows, fields)=>{
                    if(err){
                        reject(err);
                        console.log(err);
                    } else{
                        resolve({"success":true,"message":"Perangkat Telah Terdaftar"});
                        console.log("wkwkwkwk");
                        
                    }
                })
            }
        })
    })
}

function authentic(authenticData) {
    return new Promise((resolve, reject) => {
        db.query(`SELECT * FROM data_pengguna WHERE username ='${authenticData.username}'`, (error, rows, fields) => {
            if (error) {
                reject(error);
                console.log(error);
                
                dbFunc.connectionRelease;
                return;
            } else {
                if(rows!=""){
                    bcrypt.compare(authenticData.password, rows[0].password, function (err, isMatch) {
                        if (err) {
                            reject(error);
                            console.log(error);
                            
                        } else if (isMatch) {
                            resolve(rows[0]);
                            console.log(rows[0]);
                            
                        }
                        else {
                            reject({"success":false,"message":"Kata Sandi Salah"});
                        }
                    });
                } else {
                    reject({"success":false,"message":"Username Tidak Digunakan Atau Salah"});
                }
            }
        });
    });
}

function changePassword(authenticData){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_pengguna WHERE username='"+authenticData.username+"'", (error, rows, fields) => {
            if (error) {
                reject(error);
                dbFunc.connectionRelease;
                return;
            } else {
                if(rows!=""){
                    bcrypt.compare(authenticData.password, rows[0].password, function (err, isMatch) {
                        if (err) {
                            reject(error);
                            console.log(error);
                            
                        } else if (isMatch) {
                            // resolve(rows[0]);
                            bcrypt.genSalt(10, function(err, salt){
                                if(err){
                                    return next (err);
                                }else{
                                    bcrypt.hash(authenticData.new_pass, salt, function(err,hash){
                                        if(err){
                                            reject(err);
                                            console.log(err);
                                            
                                        }else{
                                            authenticData.new_pass = hash;
                                            db.query("UPDATE pengguna set password='"+authenticData.new_pass+"'",(err, rows, fields)=>{
                                                if(err){
                                                    reject(err);
                                                    console.log(err);
                                                    
                                                } else{
                                                    resolve({"success":true,"message":"Perubahan Berhasil"});
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                            
                        }
                        else {
                            reject({"success":false,"message":"Kata Sandi Salah"});
                        }
                    });
                } else {
                    reject({"success":false,"message":"Username Salah"});
                }
            }
        });
    })
}


function signup(user) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                db.query("SELECT * FROM data_pengguna WHERE username='"+user.username+"'", (error, rows, fields) => {
                    if (error) {
                        dbFunc.connectionRelease;
                        console.log(error);
                        
                        reject(error);
                    } else if(rows.length>0) {
                        dbFunc.connectionRelease;
                        reject({"success":false,"message":"user already exist ! try with different user"});
                    } else {
                        db.query("INSERT INTO data_pengguna(username,email,password)VALUES('" + user.username + "','" + user.email + "', '" + user.password + "')", (error, rows, fields) => {
                            if (error) {
                                dbFunc.connectionRelease;
                                reject(error);
                                log(error);
                            } else {
                                dbFunc.connectionRelease;
                                resolve({"success":true,"message":rows.insertId});
                                console.log(rows);
                            }
                        });
                    }
                });
            })

        });
    });
}

module.exports = authenticModel;



