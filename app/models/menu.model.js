/*jshint esversion: 6 */

const db = require('../../config/database');
const dbFunc = require('../../config/db-function');

var menuModel = {
    getAllPaketLangganan: getAllPaketLangganan,
    getPaketLanggananById: getPaketLanggananById,
    getListMakananByPaket: getListMakananByPaket,
    getAllKategoriMasakan: getAllKategoriMasakan,
    getKategoriMasakanById: getKategoriMasakanById,
    getNasiByMasakan: getNasiByMasakan,
    getLaukSatuByMasakan: getLaukSatuByMasakan,
    getLaukDuaByMasakan: getLaukDuaByMasakan,
    getBuahByMasakan: getBuahByMasakan,
    getSayurByMasakan: getSayurByMasakan,
    searchPaket: searchPaket,
    getNasiAVG: getNasiAVG,
    getLaukSatuAVG: getLaukSatuAVG,
    getLaukDuaAVG: getLaukDuaAVG,
    getSayurAVG: getSayurAVG,
    getBuahAVG: getBuahAVG,
    getLanggananNasiAvg: getLanggananNasiAvg,
    getLanggananLaukSatuAvg: getLanggananLaukSatuAvg,
    getLanggananLaukDuaAvg: getLanggananLaukDuaAvg,
    getLanggananSayurAvg: getLanggananSayurAvg,
    getLanggananBuahAvg: getLanggananBuahAvg
}

function getAllPaketLangganan(){
    return new Promise ((resolve, reject)=>{
        db.query("SELECT * FROM data_paket WHERE tipe_paket='LANGGANAN'", (error, rows, fields)=>{
            if(error) {
                dbFunc.connectionRelease;
                reject(error);
            } else {
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
            }   
        })
    });
}

function getPaketLanggananById(idpaket){
    return new Promise((resolve,reject)=>{
        db.query("SELECT * FROM data_paket WHERE tipe_paket = 'LANGGANAN' AND id_paket="+idpaket,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error)
                console.log(error);
            }else{
                dbFunc.connectionRelease;
                console.log(rows[0]);
                db.query("SELECT count(kalori) AS jumlah, avg(kalori) AS kalori, avg(berat) AS karbohidrat FROM menu_nasi",(error, nasi, fields)=>{
                    if(error){
                        dbFunc.connectionRelease;
                        reject(error);
                    }else{
                        dbFunc.connectionRelease;
                        db.query("SELECT count(kalori) AS jumlah, avg(kalori) AS kalori, avg(berat) AS protein FROM menu_lauksatu ",(error, lauksatu, fields)=>{
                            if(error){
                                dbFunc.connectionRelease;
                                reject(error);
                            }else{
                                dbFunc.connectionRelease;
                                db.query("SELECT  count(distinct nama_menu) AS jumlah, avg(distinct kalori) AS kalori, avg(distinct berat) AS protein FROM menu_laukdua",(error, laukdua, fields)=>{
                                    if(error){
                                        dbFunc.connectionRelease;
                                        reject(error);
                                    }else{
                                        dbFunc.connectionRelease;
                                        db.query("SELECT count(distinct nama_menu) AS jumlah, avg(distinct kalori) AS kalori, avg(distinct berat) AS lemak FROM menu_sayur",(error, sayur, fields)=>{
                                            if(error){
                                                dbFunc.connectionRelease;
                                                reject(error);
                                            }else{
                                                dbFunc.connectionRelease;
                                                resolve({"paket":rows[0],"nasi":nasi[0],"lauksatu":lauksatu[0],"laukdua":laukdua[0],"sayur":sayur[0] });
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
                // db.query(`SELECT nama_menu, menu_nasi.nama_nasi AS NASI, menu_lauk_satu.nama_lauksatu AS LAUK_SATU, menu_lauk_dua.nama_laukdua AS LAUK_DUA, 
                // menu_sayur.nama_sayur AS SAYUR, menu_buah.nama_buah AS BUAH FROM komponen_menu
                // JOIN menu_nasi ON komponen_menu.fk_idnasi = menu_nasi.idnasi
                // JOIN menu_lauk_satu ON komponen_menu.fk_idlauk_satu = menu_lauk_satu.idlauksatu
                // JOIN menu_lauk_dua ON komponen_menu.fk_idlauk_dua = menu_lauk_dua.idlaukdua
                // JOIN menu_sayur ON komponen_menu.fk_idsayur = menu_sayur.idsayur
                // JOIN menu_buah ON komponen_menu.fk_idbuah = menu_buah.idbuah`,(error, rows, fields)=>{
                //     if(error){
                //         dbFunc.connectionRelease;
                //         reject(error);
                //     }else{
                //         dbFunc.connectionRelease;
                //         resolve(rows);
                //     }
                // });
            }
        })
    });
}
function getListMakananByPaket(idpaket){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM Menu_Langganan WHERE fk_idpaket="+idpaket,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function orderMakananById(idpaket){
    
}

function getAllKategoriMasakan(){
    return new Promise((resolve, reject)=>{
        db.query("SELECT id_paket AS ID , NAMA_PAKET AS NAMA , image_paket AS IMG FROM data_paket WHERE tipe_paket='CUSTOM'",(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function getKategoriMasakanById(idmasakan){
    db.query("SELECT id_paket AS ID , NAMA_PAKET AS NAMA , image_paket AS IMG FROM data_paket WHERE tipe_paket='CUSTOM' AND id_paket="+idmasakan,(error, rows, fields)=>{
        if(!error){
            dbFunc.connectionRelease;
            reject(error);
        }else{
            dbFunc.connectionRelease;
            resolve(rows);
        }
    })
}

function getNasiByMasakan(idmasakan){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM menu_nasi WHERE fk_id_paket="+idmasakan,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
            }
        })
    })
}

function getLaukSatuByMasakan(idmasakan){
    return new Promise((resolve,reject)=>{
        db.query("SELECT * FROM menu_lauksatu WHERE fk_id_paket="+idmasakan,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function getLaukDuaByMasakan(idmasakan){
    return new Promise((resolve,reject)=>{
        db.query("SELECT * FROM menu_laukdua WHERE fk_id_paket="+idmasakan,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
                
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
                
            }
        })
    })
}

function getSayurByMasakan(idmasakan){
    return new Promise((resolve,reject)=>{
        db.query("SELECT * FROM menu_sayur WHERE fk_id_paket="+idmasakan,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function getBuahByMasakan(idmasakan){
    return new Promise((resolve,reject)=>{
        db.query("SELECT * FROM menu_buah WHERE fk_id_paket="+idmasakan,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
                console.log(error);
                
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
                console.log(rows);
                
            }
        })
    })
}
function getNasiAVG(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_nasi WHERE fk_id_paket="+data,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function searchPaket(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT * FROM data_paket WHERE tipe_paket = 'LANGGANAN' AND nama_paket LIKE '%"+data+"%'", (err, rows)=>{
            if(err){
                dbFunc.connectionRelease;
                reject(err);
                console.log(err);
                ;
            }
            // else if(rows=null){
            //     db.query("SELECT * FROM kategori_masakan WHERE nama_masakan LIKE="+data, (err, rows)=>{
            //         if(err){
            //             dbFunc.connectionRelease;
            //             reject(err);
            //         }
            //     })
            else{
                dbFunc.connectionRelease;
                resolve(rows)
                console.log(rows);
                
            }
        })
    })
}
function getLaukSatuAVG(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_lauksatu WHERE fk_id_paket="+data,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function getLaukDuaAVG(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_laukdua WHERE fk_id_paket="+data,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function getSayurAVG(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_sayur WHERE fk_id_paket="+data,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}

function getBuahAVG(data){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(nama_menu) AS jumlah, avg(kalori) AS kalori FROM menu_buah WHERE fk_id_paket="+data,(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function getLanggananNasiAvg(){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(kalori) AS jumlah, avg(kalori) AS kalori, avg(berat) AS karbohidrat FROM menu_nasi",(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function getLanggananLaukSatuAvg(){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(kalori) AS jumlah, avg(kalori) AS kalori, avg(berat) AS protein FROM menu_lauksatu ",(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function getLanggananLaukDuaAvg(){
    return new Promise((resolve, reject)=>{
        db.query("SELECT  count(distinct nama_menu) AS jumlah, avg(distinct kalori) AS kalori, avg(distinct berat) AS protein FROM menu_laukdua",(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function getLanggananSayurAvg(){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(distinct nama_menu) AS jumlah, avg(distinct kalori) AS kalori, avg(distinct berat) AS lemak FROM menu_sayur",(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
function getLanggananBuahAvg(){
    return new Promise((resolve, reject)=>{
        db.query("SELECT count(kalori) AS jumlah, avg(kalori) AS kalori, avg (berat) AS protein FROM menu_buah",(error, rows, fields)=>{
            if(error){
                dbFunc.connectionRelease;
                reject(error);
            }else{
                dbFunc.connectionRelease;
                resolve(rows);
            }
        })
    })
}
module.exports = menuModel;